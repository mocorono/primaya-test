@extends('layouts.app')
@section('content')
<div class="container">
    <div class="justify-content-center">
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div>
        @endif
        <div class="card" id="card">
            <div class="card-header">Daftar Menu
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModalAdd">
                    Add Menu
                </button>
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Nama Menu</th>
                            <th width="280px">Status</th>
                            <th width="280px">Harga</th>
                            <th width="280px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $key => $post)
                            <tr>
                                <td>{{ $post->id }}</td>
                                <td>{{ $post->namamenu }}</td>
                                <td>{{ $post->statusmenu }}</td>
                                <td>{{ $post->harga }}</td>
                                <td>
                                    <a class="btn btn-success" href="{{ route('daftarmenus.show',$post->id) }}">Show</a>
                                        <a class="btn btn-primary" href="{{ route('daftarmenus.edit',$post->id) }}">Edit</a>
                                        {!! Form::open(['method' => 'DELETE','route' => ['daftarmenus.destroy', $post->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $data->appends($_GET)->links() }}
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModalAdd">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form id="contactForm">
                    <div class="form-group mb-3">
                        <label>Nama Menu</label>
                        <input type="text" name="namamenu" class="form-control" placeholder="Enter Nama Menu" id="namamenu">                        
                    </div>
                    <div class="form-group mb-3"> 
                        <label>Status Menu</label>
                        <select class="form-select" name="statusmenu" id="statusmenu">
                            <option value="active">Active</option>
                            <option value="notactive">Not Active</option>
                        </select>
                    </div>
                    <div class="form-group mb-3">
                        <label>Harga</label>
                        <input type="text" name="harga" class="form-control" placeholder="Harga" id="harga">                        
                    </div>
                    <div class="form-group mb-3">
                        <button class="btn btn-success submitaddmenu">Submit</button>
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection