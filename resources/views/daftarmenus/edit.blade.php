@extends('layouts.app')
@section('content')
<div class="container">
    <div class="justify-content-center">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">
            <div class="card-header">Create Menu
                <span class="float-right">
                    <a class="btn btn-primary" href="{{ route('daftarmenus.index') }}">Menu</a>
                </span>
            </div>
            <div class="card-body">
                {!! Form::model($data, ['route' => ['daftarmenus.update', $data->id], 'method'=>'PATCH']) !!}
                    <div class="form-group">
                        <strong>Nama Menu:</strong>
                        {!! Form::text('namamenu', null, array('placeholder' => 'Nama Menu','class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        <strong>Status Menu:</strong>
                        {!! Form::text('statusmenu', null, array('placeholder' => 'Status Menu','class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        <strong>Harga:</strong>
                        {!! Form::text('harga', null, array('placeholder' => 'Body','class' => 'form-control')) !!}
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection