@extends('layouts.app')
@section('content')
<div class="container">
    <div class="justify-content-center">
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div>
        @endif
        <div class="card" id="card">
            <div class="card-header">Users
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModalAddUser">
                    Add User
                </button>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Roles</th>
                            <th width="280px">Action</th> 
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['user'] as $key => $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @if(!empty($user->getRoleNames()))
                                        @foreach($user->getRoleNames() as $val)
                                            <label class="badge badge-dark">{{ $val }}</label>
                                        @endforeach
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-success" href="{{ route('users.show',$user->id) }}">Show</a>
                                        <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
                                        {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $data['user']->render() }}
            </div>
        </div>
    </div>
</div>


<div class="modal" id="myModalAddUser">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form id="contactForm">
                    <div class="form-group mb-3">
                        <label>Names</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter Name" id="name">                        
                    </div>
                    <div class="form-group mb-3">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control" placeholder="Enter Email" id="email">                        
                    </div>
                    <div class="form-group mb-3">
                        <label>Password</label>
                        <input type="text" name="password" class="form-control" placeholder="Password" id="password">                        
                    </div>
                    <div class="form-group mb-3">
                        <label>Confirm Password</label>
                        <input type="text" name="password_confirmation" class="form-control" placeholder="Password Confirmation" id="password_confirmation">                        
                    </div>
                    <div class="form-group mb-3"> 
                        <label>Role</label>
                        <select class="form-select" name="roles" id="roles">
                            @foreach($data['roles'] as $val)
                                @if($val != 'superadmin')
                                    <option value="pelayan">{{$val}}</option>
                                @endif                                
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mb-3">
                        <button class="btn btn-success submitadduser">Submit</button>
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection