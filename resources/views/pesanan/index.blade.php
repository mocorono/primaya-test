@extends('layouts.app')
@section('content')
<div class="container">
    <div class="justify-content-center">
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div>
        @endif
        <div class="card" id="card">
            <div class="card-header">Daftar Pesanan
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModalAdd">
                    Add Pesanan
                </button>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Kode Pemesanan</th>
                            <th>Nomor Meja</th>
                            <th>Status Pesanan</th>
                            <th width="280px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['pesan'] as $key => $val)
                            <tr>
                                <td>{{ $val->id }}</td>
                                <td>{{ $val->kode_pesanan }}</td>
                                <td>{{ $val->nomormeja }}</td>
                                <td>{{ $val->status_pesanan }}</td>
                                <td>
                                    <a class="btn btn-warning" href="{{ route('pesanan.edit',$val->id) }}">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $data['pesan']->render() }}
            </div>
        </div>
    </div>
</div>


<div class="modal" id="myModalAdd">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form id="formpesan">
                    <div class="form-group mb-3"> 
                        <label>Meja</label>
                        <select class="form-select" name="nomormeja" id="nomormeja">
                            @foreach($data['meja'] as $val)
                                <option value="{{$val->id}}">{{$val->nomormeja}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mb-3">
                        <label>Menu</label><br>
                        @foreach($data['menu'] as $val)
                            <div style="width: 40%; display: inline-block;">
                                @if($val->statusmenu == 'active')
                                    <input id="idmenu" type="checkbox" name="idmenu" value="{{$val->id}}"><font style="margin-left:3px;">{{$val->namamenu}} </font> 
                                @else
                                    -- <font style="margin-left:3px;">{{$val->namamenu}} </font> <font style="margin-left:3px; color: red;">Not Active </font>  
                                @endif                                       
                            </div>
                        @endforeach
                           
                    </div>
                    <div class="form-group mb-3">
                        <button class="btn btn-success submitaddpesanan">Submit</button>
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection