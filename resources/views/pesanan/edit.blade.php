@extends('layouts.app')
@section('content')
<div class="container">
    <div class="justify-content-center">
        <div class="card">
            <div class="card-header">Update Pesanan
                <span class="mt-3" style="display: block;">
                    Kode Pesanan {{$data['pesanan'][0]->kode_pesanan}}
                </span>
            </div>

            @php
                $idPesan = $data['pesanan'][0]->id;
            @endphp

            <div class="card-body">
                {!! Form::model($data['pesanan'][0], ['route' => ['pesanan.update', $data['pesanan'][0]->id], 'method'=>'PATCH']) !!}
                    <input name="_method" type="hidden" value="PATCH">
                    @csrf
                    <input type="hidden" name="kode_pesanan" value="{{$data['pesanan'][0]->kode_pesanan}}">
                    <div class="form-group mb-3"> 
                        <label>Meja</label>
                        <select class="form-select" name="id_meja" id="nomormeja">
                            @foreach($data['meja'] as $val)
                                <option value="{{$val->id}}" <?= ($data['pesanan'][0]->id_meja == $val->id)?'selected':''; ?>>{{$val->nomormeja}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mb-3">
                        <label>Menu</label><br>
                        @foreach($data['menu'] as $val)
                            <div style="width: 40%; display: inline-block;">
                                @if($val->statusmenu == 'active')
                                    <?php
                                    if(in_array($val->id, $data['trx_pesanans'])){
                                        $selc = 'checked';
                                    }
                                    else{
                                        $selc = '';
                                    }
                                    ?>
                                    <input id="idmenu" type="checkbox" name="idmenu[]" value="{{$val->id}}" <?= $selc; ?>><font style="margin-left:3px;">{{$val->namamenu}} </font> 
                                @else
                                    -- <font style="margin-left:3px;">{{$val->namamenu}} </font> <font style="margin-left:3px; color: red;">Not Active </font>  
                                @endif                                       
                            </div>
                        @endforeach
                           
                    </div>
                    <div class="form-group mb-3">
                        <button class="btn btn-success " name='mybutton' value="save" alt="Save" type="submit">Update</button>
                        <button class="btn btn-warning closeaddpesanan" value="close" alt="Close" name='mybutton'>Pay and Close Order</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection