<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="{{ asset('custom/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <a class="navbar-brand" href="{{ route('users.index') }}">
                    User
                </a>
                <a class="navbar-brand" href="{{ route('daftarmenus.index') }}">
                    Daftar Menu
                </a>
                <a class="navbar-brand" href="{{ route('daftarmeja.index') }}">
                    Daftar Meja
                </a>
                <a class="navbar-brand" href="{{ route('pesanan.index') }}">
                    Pesanan
                </a>
                <!--  -->
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <!-- <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown"> -->
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                <!-- </div> -->
                            </li>
                        @endguest
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
<script type="text/javascript">
    $( document ).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let _token   = $('meta[name="csrf-token"]').attr('content');

        $(".submitadduser").click(function(e){
            e.preventDefault();

            var name = $("input[name=name]").val();
            var password = $("input[name=password]").val();
            var email = $("input[name=email]").val();
            var password_confirmation = $("input[name=password_confirmation]").val();
            var roles = $("#roles").val();

            $.ajax({
               type:'POST',
               url:"{{ route('users.store') }}",
               data:{_token: _token, name:name, password:password, email:email, password_confirmation:password_confirmation, roles:roles},
               success:function(data){
                  $('#myModalAddUser').modal('hide');
                  $("#card").load(location.href + " #card");
               }
            });
        });

        $(".submitaddmenu").click(function(e){
            e.preventDefault();

            var namamenu = $("input[name=namamenu]").val();
            var harga = $("input[name=harga]").val();
            var statusmenu = $("#statusmenu").val();

            $.ajax({
               type:'POST',
               url:"{{ route('daftarmenus.store') }}",
               data:{_token: _token, namamenu:namamenu, harga:harga, statusmenu:statusmenu},
               success:function(data){
                  $('#myModalAdd').modal('hide');
                  $("#card").load(location.href + " #card");
               }
            });
        });

        $(".submitaddmeja").click(function(e){
            e.preventDefault();

            var nomormeja = $("input[name=nomormeja]").val();

            $.ajax({
               type:'POST',
               url:"{{ route('daftarmeja.store') }}",
               data:{_token: _token, nomormeja:nomormeja},
               success:function(data){
                  $('#myModalAdd').modal('hide');
                  $("#card").load(location.href + " #card");
               }
            });
        });

        $(".submitaddpesanan").click(function(e){
            e.preventDefault();

            var idmenus= [];

            $("input:checkbox[name=idmenu]:checked").each(function(){
              idmenus.push($(this).val());
            });

            idmenu= JSON.stringify(idmenus);
            var id_meja = $("#nomormeja").val();
            console.log(id_meja);

            $.ajax({
               type:'POST',
               url:"{{ route('pesanan.store') }}",
               data:{_token: _token, id_meja:id_meja, idmenu:idmenu},
               success:function(data){
                  $('#myModalAdd').modal('hide');
                  $("#card").load(location.href + " #card");
               }
            });
        });

    });

</script>
</html>
