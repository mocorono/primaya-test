@extends('layouts.app')
@section('content')
<div class="container">
    <div class="justify-content-center">
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div>
        @endif
        <div class="card" id="card">
            <div class="card-header">Daftar Meja
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#myModalAdd">
                    Add Meja
                </button>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Nomor Meja</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $key => $val)
                            <tr>
                                <td>{{ $val->id }}</td>
                                <td>{{ $val->nomormeja }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $data->render() }}
            </div>
        </div>
    </div>
</div>


<div class="modal" id="myModalAdd">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form id="contactForm">
                    <div class="form-group mb-3">
                        <label>Nomor Meja</label>
                        <input type="number" name="nomormeja" class="form-control" placeholder="Nomor Meja" id="nomormeja">                        
                    </div>
                    <div class="form-group mb-3">
                        <button class="btn btn-success submitaddmeja">Submit</button>
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection