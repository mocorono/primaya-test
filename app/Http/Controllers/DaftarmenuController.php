<?php

namespace App\Http\Controllers;

use App\Models\Daftar_menu;
use Illuminate\Http\Request;

class DaftarmenuController extends Controller
{
    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct()
    {
        // Change to role, not permission
         // $this->middleware('permission:daftarmenu-list|daftarmenu-create|daftarmenu-edit|daftarmenu-delete', ['only' => ['index', 'show']]);
         // $this->middleware('permission:daftarmenu-create', ['only' => ['create', 'store']]);
         // $this->middleware('permission:daftarmenu-edit', ['only' => ['edit', 'update']]);
         // $this->middleware('permission:daftarmenu-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Daftar_menu::latest()->paginate(5);

        return view('daftarmenus.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('daftarmenus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'namamenu' => 'required',
            'harga' => 'required',
        ]);

        $input = $request->except(['_token']);
    
        Daftar_menu::create($input);
    
        return redirect()->route('daftarmenus.index')
            ->with('success','Menu created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Daftar_menu::find($id);

        return view('daftarmenus.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Daftar_menu::find($id);

        return view('daftarmenus.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'namamenu' => 'required',
            'harga' => 'required',
        ]);

        $data = Daftar_menu::find($id);
    
        $data->update($request->all());
    
        return redirect()->route('daftarmenus.index')
            ->with('success', 'Menu updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Daftar_menu::find($id)->delete();
    
        return redirect()->route('daftarmenus.index')
            ->with('success', 'Menu deleted successfully.');
    }
}
