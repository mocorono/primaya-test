<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Models\Daftar_menu;
use App\Models\Daftarmeja;
use App\Models\Pesanan;
use App\Models\Trx_pesanan;
use App\Models\User;

class PesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pesan'] = DB::table('pesanans')
            ->join('trx_pesanans', 'pesanans.kode_pesanan', 'trx_pesanans.id_pesanan')
            ->join('daftarmejas', 'trx_pesanans.id_meja', 'daftarmejas.id')
            ->join('daftar_menus', 'trx_pesanans.id_menu', 'daftar_menus.id')
            ->select('pesanans.*', 'trx_pesanans.id as id_trx,', 'daftarmejas.id as id_meja', 'daftarmejas.nomormeja', 'daftar_menus.id as id_menu')
            ->groupBy('pesanans.kode_pesanan')
            ->latest('pesanans.created_at')->paginate(5);

        $data['menu'] = Daftar_menu::orderBy('id', 'desc')->get();   
        $data['meja'] = Daftarmeja::orderBy('id', 'desc')->get();   

        return view('pesanan.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uid = uniqid();
        $uidpesanan = DB::table('pesanans')->latest('created_at')->first();
        (!empty($uidpesanan))?$idpesan=$uidpesanan->id:$idpesan=1;

        $input = $request->all();

        $input['id_pesanan'] = 'PSN'.date('Ymd').($idpesan+1);
        $inputs['kode_pesanan'] = 'PSN'.date('Ymd').($idpesan+1);
        $inputs['status_pesanan'] = 'active';
        // $input['id_meja'] = $input['id_meja'];        
        $idmenus = json_decode($input['idmenu'],true);
        foreach($idmenus as $val){            
            $input['id_menu'] = $val;  
            $input['id_meja'] = $input['id_meja']; 
            $insert = Trx_pesanan::create($input);
        }

        Pesanan::create($inputs);

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['pesanan'] = DB::table('pesanans')
            ->join('trx_pesanans', 'pesanans.kode_pesanan', 'trx_pesanans.id_pesanan')
            ->join('daftarmejas', 'trx_pesanans.id_meja', 'daftarmejas.id')
            ->join('daftar_menus', 'trx_pesanans.id_menu', 'daftar_menus.id')
            ->select('pesanans.*', 'trx_pesanans.id as id_trx,', 'daftarmejas.id as id_meja', 'daftarmejas.nomormeja', 'daftar_menus.id as id_menu')
            ->where('pesanans.id', '=', $id)
            ->get();


        $data['menu'] = Daftar_menu::orderBy('id', 'desc')->get();   
        $data['meja'] = Daftarmeja::orderBy('id', 'desc')->get();   
        $datapesanan = Trx_pesanan::orderBy('id', 'desc')
        ->where('id_pesanan', '=', $data['pesanan'][0]->kode_pesanan)
        ->get()->toArray();   

        $i = 0;
        foreach($datapesanan as $val){
            $data['trx_pesanans'][$i] = $val['id_menu'];
            $i++;
        }
    
        return view('pesanan.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $pesanan = Pesanan::find($id);
        if($input['mybutton'] == 'save'){
            $pesanan->update($input);
        
            DB::table('trx_pesanans')
                ->where('id_pesanan', '=', $input['kode_pesanan'])
                ->delete();

            // dd($input);
            
            foreach($input['idmenu'] as $val){
                $inputs['id_menu'] = $val;  
                $inputs['id_pesanan'] = $input['kode_pesanan'];  
                $inputs['id_meja'] = $input['id_meja'];             
                $insert = Trx_pesanan::create($inputs);
            }
        }
        else{
            $input['status_pesanan'] = 'close';  
            $pesanan->update($input);
        }
        
    
        return redirect()->route('pesanan.index')
            ->with('success', 'Pesanan updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
