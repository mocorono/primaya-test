<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Daftar_menu extends Model
{
    use HasFactory;

    protected $fillable = [
        'namamenu',
        'statusmenu',
        'harga',
    ];
}
