<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trx_pesanan extends Model
{
    use HasFactory;

    protected $fillable = [
        'uidpesanan',
        'id_pesanan',        
        'id_menu',
        'id_meja',
    ];
}
