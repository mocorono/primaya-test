-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2022 at 04:54 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projecttest`
--

-- --------------------------------------------------------

--
-- Table structure for table `daftarmejas`
--

CREATE TABLE `daftarmejas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nomormeja` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `daftarmejas`
--

INSERT INTO `daftarmejas` (`id`, `nomormeja`, `created_at`, `updated_at`) VALUES
(1, 1, '2022-02-25 02:18:35', '2022-02-25 02:18:35'),
(2, 13, '2022-02-25 02:19:32', '2022-02-25 02:19:32'),
(3, 7, '2022-02-25 07:49:07', '2022-02-25 07:49:07');

-- --------------------------------------------------------

--
-- Table structure for table `daftar_menus`
--

CREATE TABLE `daftar_menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `namamenu` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `statusmenu` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `daftar_menus`
--

INSERT INTO `daftar_menus` (`id`, `namamenu`, `statusmenu`, `harga`, `created_at`, `updated_at`) VALUES
(1, 'Menu 1', 'active', 200000, '2022-02-25 01:38:28', '2022-02-25 01:38:28'),
(3, 'Menu 2', 'active', 400000, '2022-02-25 03:10:36', '2022-02-25 05:22:42'),
(4, 'Menu 3', 'active', 400000, '2022-02-25 03:11:11', '2022-02-25 03:28:31'),
(5, 'Menu 5', 'active', 10000, '2022-02-25 03:49:26', '2022-02-25 06:59:02');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_02_25_033431_create_permission_tables', 1),
(6, '2022_02_25_033627_create_daftar_menu', 2),
(7, '2022_02_25_085612_create_daftarmeja_table', 3),
(8, '2022_02_25_092109_create_pesanan_table', 4),
(9, '2022_02_25_092605_create_pesanans_table', 5),
(10, '2022_02_25_092936_create_trx_pesanan_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(2, 'App\\Models\\User', 3),
(2, 'App\\Models\\User', 5),
(3, 'App\\Models\\User', 4),
(3, 'App\\Models\\User', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'user-list', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39'),
(2, 'user-create', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39'),
(3, 'user-edit', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39'),
(4, 'user-delete', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39'),
(5, 'role-list', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39'),
(6, 'role-create', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39'),
(7, 'role-edit', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39'),
(8, 'role-delete', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39'),
(9, 'permission-list', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39'),
(10, 'permission-create', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39'),
(11, 'permission-edit', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39'),
(12, 'permission-delete', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39'),
(13, 'daftarmenus-list', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39'),
(14, 'daftarmenus-create', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39'),
(15, 'daftarmenus-edit', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39'),
(16, 'daftarmenus-delete', 'web', '2022-02-24 22:52:39', '2022-02-24 22:52:39');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, 'token-auth', '3a75a729d54cf282b148f57eae49a1d7311d9eed31b4b9a50dce73716270690c', '[\"*\"]', NULL, '2022-02-25 08:34:34', '2022-02-25 08:34:34'),
(2, 'App\\Models\\User', 1, 'token-auth', '43b7d24305b7066085454439ba93867a047e638e03b741de0cbc68ac0da4cffe', '[\"*\"]', '2022-02-25 08:47:11', '2022-02-25 08:45:58', '2022-02-25 08:47:11'),
(3, 'App\\Models\\User', 1, 'token-auth', '8d095b044ebe8402c97f2546c27fc187723bd88c9e8f4b1a28e230aa0cdb371a', '[\"*\"]', NULL, '2022-02-25 08:48:06', '2022-02-25 08:48:06'),
(4, 'App\\Models\\User', 1, 'token-auth', '9826a22e3d378f339206050eeb79daf218794ca9044db17e0f14db239203590b', '[\"*\"]', NULL, '2022-02-25 08:54:03', '2022-02-25 08:54:03');

-- --------------------------------------------------------

--
-- Table structure for table `pesanans`
--

CREATE TABLE `pesanans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_pesanan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `totalbayar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_bayar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_pesanan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pesanans`
--

INSERT INTO `pesanans` (`id`, `kode_pesanan`, `totalbayar`, `status_bayar`, `status_pesanan`, `created_at`, `updated_at`) VALUES
(1, '5', NULL, NULL, 'open', '2022-02-25 04:49:31', '2022-02-25 04:49:31'),
(2, '202202252', NULL, NULL, 'open', '2022-02-25 04:58:49', '2022-02-25 04:58:49'),
(3, '202202253', NULL, NULL, 'open', '2022-02-25 04:59:04', '2022-02-25 04:59:04'),
(4, 'PSN202202254', NULL, NULL, 'active', '2022-02-25 05:17:19', '2022-02-25 05:17:19'),
(5, 'PSN202202255', NULL, NULL, 'active', '2022-02-25 05:17:29', '2022-02-25 05:17:29'),
(6, 'PSN202202256', NULL, NULL, 'active', '2022-02-25 05:17:34', '2022-02-25 05:17:34'),
(7, 'PSN202202257', NULL, NULL, 'active', '2022-02-25 05:23:01', '2022-02-25 05:23:01'),
(8, 'PSN202202258', NULL, NULL, 'close', '2022-02-25 05:23:55', '2022-02-25 07:30:04'),
(9, 'PSN202202259', NULL, NULL, 'active', '2022-02-25 05:24:04', '2022-02-25 05:24:04'),
(10, 'PSN2022022510', NULL, NULL, 'active', '2022-02-25 05:24:10', '2022-02-25 05:24:10'),
(11, 'PSN2022022511', NULL, NULL, 'active', '2022-02-25 06:34:23', '2022-02-25 06:34:23'),
(12, 'PSN2022022512', NULL, NULL, 'active', '2022-02-25 06:35:02', '2022-02-25 06:35:02'),
(13, 'PSN2022022513', NULL, NULL, 'active', '2022-02-25 06:35:04', '2022-02-25 06:35:04'),
(14, 'PSN2022022514', NULL, NULL, 'active', '2022-02-25 06:35:26', '2022-02-25 06:35:26'),
(15, 'PSN2022022515', NULL, NULL, 'active', '2022-02-25 06:35:30', '2022-02-25 06:35:30');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'web', '2022-02-24 23:16:41', '2022-02-24 23:16:41'),
(2, 'pelayan', 'web', '2022-02-24 23:18:17', '2022-02-24 23:18:17'),
(3, 'kasir', 'web', '2022-02-24 23:18:30', '2022-02-24 23:18:30');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(13, 2),
(13, 3),
(14, 1),
(14, 2),
(14, 3),
(15, 1),
(15, 2),
(15, 3),
(16, 1),
(16, 2),
(16, 3);

-- --------------------------------------------------------

--
-- Table structure for table `trx_pesanans`
--

CREATE TABLE `trx_pesanans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_pesanan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_menu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_meja` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trx_pesanans`
--

INSERT INTO `trx_pesanans` (`id`, `id_pesanan`, `id_menu`, `id_meja`, `created_at`, `updated_at`) VALUES
(18, '3', '5', '2', '2022-02-25 04:38:39', '2022-02-25 04:38:39'),
(19, '3', '4', '2', '2022-02-25 04:38:39', '2022-02-25 04:38:39'),
(20, '4', '5', '2', '2022-02-25 04:48:31', '2022-02-25 04:48:31'),
(21, '4', '4', '2', '2022-02-25 04:48:31', '2022-02-25 04:48:31'),
(22, '5', '5', '2', '2022-02-25 04:49:31', '2022-02-25 04:49:31'),
(23, '5', '4', '2', '2022-02-25 04:49:31', '2022-02-25 04:49:31'),
(24, '202202252', '5', '1', '2022-02-25 04:58:49', '2022-02-25 04:58:49'),
(25, '202202252', '1', '1', '2022-02-25 04:58:49', '2022-02-25 04:58:49'),
(26, '202202253', '5', '1', '2022-02-25 04:59:04', '2022-02-25 04:59:04'),
(27, '202202253', '4', '1', '2022-02-25 04:59:04', '2022-02-25 04:59:04'),
(28, '202202253', '1', '1', '2022-02-25 04:59:04', '2022-02-25 04:59:04'),
(29, 'PSN202202254', '5', '2', '2022-02-25 05:17:19', '2022-02-25 05:17:19'),
(30, 'PSN202202255', '5', '1', '2022-02-25 05:17:29', '2022-02-25 05:17:29'),
(31, 'PSN202202255', '1', '1', '2022-02-25 05:17:29', '2022-02-25 05:17:29'),
(32, 'PSN202202256', '5', '1', '2022-02-25 05:17:34', '2022-02-25 05:17:34'),
(33, 'PSN202202256', '1', '1', '2022-02-25 05:17:34', '2022-02-25 05:17:34'),
(34, 'PSN202202257', '5', '2', '2022-02-25 05:23:01', '2022-02-25 05:23:01'),
(35, 'PSN202202257', '3', '2', '2022-02-25 05:23:01', '2022-02-25 05:23:01'),
(36, 'PSN202202257', '1', '2', '2022-02-25 05:23:01', '2022-02-25 05:23:01'),
(52, NULL, '5', '1', '2022-02-25 07:20:44', '2022-02-25 07:20:44'),
(53, NULL, '3', '1', '2022-02-25 07:20:44', '2022-02-25 07:20:44'),
(54, NULL, '1', '1', '2022-02-25 07:20:44', '2022-02-25 07:20:44'),
(55, NULL, '5', '1', '2022-02-25 07:21:02', '2022-02-25 07:21:02'),
(56, NULL, '3', '1', '2022-02-25 07:21:02', '2022-02-25 07:21:02'),
(57, NULL, '1', '1', '2022-02-25 07:21:02', '2022-02-25 07:21:02'),
(58, NULL, '5', '1', '2022-02-25 07:21:57', '2022-02-25 07:21:57'),
(59, NULL, '4', '1', '2022-02-25 07:21:57', '2022-02-25 07:21:57'),
(60, NULL, '1', '1', '2022-02-25 07:21:57', '2022-02-25 07:21:57'),
(72, 'PSN202202258', '5', '2', '2022-02-25 07:31:23', '2022-02-25 07:31:23'),
(73, 'PSN202202258', '4', '2', '2022-02-25 07:31:23', '2022-02-25 07:31:23'),
(74, 'PSN202202258', '3', '2', '2022-02-25 07:31:23', '2022-02-25 07:31:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin13', 'admin@mail.com', NULL, '$2y$10$qb.SwUJgJy.c3D/y8rJt8OOM6uvwhxeEuLI4Vedgi3tUwMlsHv9Zm', 's7WLL7TkLsqrDQYIMy7ydmpUTduZzAt5ei89HfwzUlBKB5rtzBvEZ8owoHNj', '2022-02-24 22:52:52', '2022-02-24 22:52:52'),
(3, 'pelayan 1', 'pelayan1@mail.com', NULL, '$2y$10$jUb8FOjhPRS6h.BRGCoZFe4bVJ.Eil/DnODdnFzemRIHjEjdFZ06m', NULL, '2022-02-24 23:19:10', '2022-02-24 23:19:10'),
(4, 'kasir 1', 'kasir1@mail.com', NULL, '$2y$10$D/HZjOO.ktlq74TXUVDv5uMafBrm84BKth5ERLEo02zNJwRab0syW', NULL, '2022-02-24 23:19:30', '2022-02-24 23:19:30'),
(5, 'Pelayan 2', 'pelayan2@mail.com', NULL, '$2y$10$pjub5btZ67ZVwxFpg1C.F.KyfCQH0MpyWWw0MFQxjiBOwV8nrVxs.', NULL, '2022-02-25 01:26:19', '2022-02-25 01:26:19'),
(6, 'Kasir 2', 'kasir2@mail.com', NULL, '$2y$10$DpWtw7v2tMro8ftms7jTfetYGLKyzM2prOXCa1u9Ytb5D6rbjkws2', NULL, '2022-02-25 01:29:43', '2022-02-25 01:29:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daftarmejas`
--
ALTER TABLE `daftarmejas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daftar_menus`
--
ALTER TABLE `daftar_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `pesanans`
--
ALTER TABLE `pesanans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `trx_pesanans`
--
ALTER TABLE `trx_pesanans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daftarmejas`
--
ALTER TABLE `daftarmejas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `daftar_menus`
--
ALTER TABLE `daftar_menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pesanans`
--
ALTER TABLE `pesanans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `trx_pesanans`
--
ALTER TABLE `trx_pesanans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
